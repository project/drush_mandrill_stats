# Drush Mandrill Statistics

This project provides drush commands to fetch data from the Mandrill
transactional email service, to display email tags and statistics on each tag.
Tags represent different types of transactional email sent via Mandrill.

## Install

From a command line, run the following:

    $ drush dl drush_mandrill_stats

This will download the project and place it in your $HOME/.drush folder.
Alternatively, you may also download the release manually from Drupal.org.
