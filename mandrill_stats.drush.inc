<?php
/**
 * @file
 * Drush commands for mandrill_stats.module.
 */

/**
 * Implements hook_drush_command().
 */
function mandrill_stats_drush_command() {
  $items['mandrill-tags-list'] = array(
    'description' => 'List all the user-defined tags associated with Mandrill transactional email.',
    'drupal dependencies' => array('mandrill'),
    'aliases' => array('mtl'),
  );
  $items['mandrill-tags-info'] = array(
    'description' => 'Fetch detailed information about a single Mandrill tag, including aggregates of recent statistics.',
    'arguments' => array(
      'tag' => 'User-defined Mandrill tag to fetch statistics for.',
    ),
    'options' => array(
      'stats' => array(
        'description' => 'Statistics to display. (Defaults to all).',
        'example-value' => 'sent, hard_bounces, soft_bounces, rejects, complains, unsubs, opens, unique_opens, clicks, unique_slicks',
      ),
      'ranges' => array(
        'description' => 'Time frame to display. (Defaults to all).',
        'example-value' => 'today, last_7_days, last_30_days, last_60_days, last_90_days, total',
      ),
      'table' => array(
        'description' => 'Output in tabular format.',
      ),
    ),
    'drupal dependencies' => array('mandrill'),
    'aliases' => array('mti'),
  );
  return $items;
}

/**
 * List all the user-defined tags associated with Mandrill transactional email.
 */
function drush_mandrill_stats_mandrill_tags_list() {
  $mandrill = mandrill_get_api_object();
  $lists = $mandrill->tags->getList();
  foreach ($lists as $list) {
    // The "all" tag isn't returning valid statistics, so don't bother listing it.
    if ($list['tag'] !== 'all') {
      drush_print($list['tag']);
    }
  }
}

/**
 * Fetch information about a Mandrill tag.
 */
function drush_mandrill_stats_mandrill_tags_info($tag = 'all') {
  $mandrill = mandrill_get_api_object();
  $info = $mandrill->tags->info($tag);

  drush_print('TAG: ' . array_shift($info));
  drush_print('REPUTATION: ' . $info['reputation'] . PHP_EOL);

  foreach ($info['stats']['today'] as $key => $value) {
    // Get field names from keys in the $info['stats']['today'] array.
    $stat_keys[$key] = $key;
    // Mandrill returns totals at the top level of the array, with smaller time
    // frames in nested arrays. Copy totals to a nested array for easier output.
    if (!is_array($info[$key])) {
      // Look to nested array for key order. Later, drush_print_table prints
      // associative arrays as if they were sequential, so the totals were
      // appearing out-of-order when output as a table.
      $info['stats']['total'][$key] = $info[$key];
    }
  }

  // If user specifies ranges, only show those ranges.
  if ($ranges = drush_get_option('ranges')) {
    if (!empty($ranges)) {
      $info['stats'] = array_intersect_key($info['stats'], array_flip(explode(',', $ranges)));
    }
  }

  // If user specifies stats to display, only show those.
  if ($stat_types = drush_get_option('stats')) {
    foreach ($info['stats'] as $period => $stats) {
      $info['stats'][$period] = array_intersect_key($info['stats'][$period], array_flip(explode(',', $stat_types)));
    }
    $stat_keys = array_intersect($stat_keys, explode(',', $stat_types));
  }

  if ($display_table = drush_get_option('table', FALSE)) {
    $header = str_replace('_', ' ', array_map('strtoupper', $stat_keys));
    // Add header for first column.
    array_unshift($header, 'PERIOD');

    // Reformat period labels.
    foreach ($info['stats'] as $period => $stats) {
      array_unshift($info['stats'][$period], str_replace('days', '', str_replace('_', ' ', $period)));
    }

    array_unshift($info['stats'], $header);
    drush_print_table($info['stats'], TRUE);

  } else {
    foreach ($info['stats'] as $period => $stats) {
      drush_print(strtoupper(str_replace('_', ' ', $period)));
      if (is_array($stats)) {
        foreach ($stats as $key => $value) {
          drush_print(str_replace('_', ' ', $key) . ': ' . $value);
        }
      print(PHP_EOL);
      }
    }
  }
}
